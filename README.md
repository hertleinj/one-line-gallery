# One Line Gallery

A Gallery in One Line.

Example under: https://hertleinj.gitlab.io/post/wolfensteinneumarkt/

### Installing

*  Put the olg.html file under layouts/shortcodes 
*  Place the shortcode and add the attributes

Example Use:
3 Images Gallery 2 in one line 1 in the next.
``
{{<olg src="/img/DSF2777.jpg,/img/wolfstein/_A020733-min.jpg" width="80%" elwidth="48">}}
``

## Attributes


*  src:  Location to images one or multiple with ","-Seperation
*  width:  The Width of all images in Percent
* elwidth:  The Width of one images **optional** 


## Use With

* [Hugo](https://gohugo.io) - Hugo Static Website Generator

## Contributing

Just fork or create Pull Request.

## Authors

* **Jan Hertlein** - *Initial work* - [One Line Gallery](https://gitlab.com/hertleinj/one-line-gallery)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
